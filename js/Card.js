//drag drop code taken from http://answers.unity3d.com/questions/187321/simple-drag-and-drop-no-physics.html

private var screenPoint: Vector3;
private var offset: Vector3;
private var curScreenPoint : Vector3;
private var curPosition : Vector3;
private var prevPosition : Vector3;
private var delta : Vector3;
private var lift = .25;
private var isChecked = false;
private var cardPile = new Array();
private var liftPile = false;
private var movePile = false;
private var doDrop = false;
private var canCreatePile = true;
private var isMoving = false;
private var isInHand = false;
private var faceDownAngle : Quaternion;

public var spades: Texture[];
public var clubs: Texture[];
public var hearts: Texture[];
public var diamonds: Texture[];

//import network manager code
private var networkManage : GameObject; 
networkManage = GameObject.Find('Floor');

private static var spadeAce: Array = [0,0];
//private static var spadeTwo: Array = [0,1];
//private static var spadeThree: Array = [0,2];
//private static var spadeFour: Array = [0,3];
//private static var spadeFive: Array = [0,4];
//private static var spadeSix: Array = [0,5];
//private static var spadeSeven: Array = [0,6];
//private static var spadeEight: Array = [0,7];
private static var spadeNine: Array = [0,8];
private static var spadeTen: Array = [0,9];
private static var spadeJack: Array = [0,10];
private static var spadeQueen: Array = [0,11];
private static var spadeKing: Array = [0,12]; 

private static var diamondAce: Array = [1,0];
//private static var diamondTwo: Array = [1,1];
//private static var diamondThree: Array = [1,2];
//private static var diamondFour: Array = [1,3];
//private static var diamondFive: Array = [1,4];
//private static var diamondSix: Array = [1,5];
//private static var diamondSeven: Array = [1,6];
//private static var diamondEight: Array = [1,7];
private static var diamondNine: Array = [1,8];
private static var diamondTen: Array = [1,9];
private static var diamondJack: Array = [1,10];
private static var diamondQueen: Array = [1,11];
private static var diamondKing: Array = [1,12];

private static var clubAce: Array = [2,0];
//private static var clubTwo: Array = [2,1];
//private static var clubThree: Array = [2,2];
//private static var clubFour: Array = [2,3];
//private static var clubFive: Array = [2,4];
//private static var clubSix: Array = [2,5];
//private static var clubSeven: Array = [2,6];
//private static var clubEight: Array = [2,7];
private static var clubNine: Array = [2,8];
private static var clubTen: Array = [2,9];
private static var clubJack: Array = [2,10];
private static var clubQueen: Array = [2,11];
private static var clubKing: Array = [2,12];

private static var heartAce: Array = [3,0];
//private static var heartTwo: Array = [3,1];
//private static var heartThree: Array = [3,2];
//private static var heartFour: Array = [3,3];
//private static var heartFive: Array = [3,4];
//private static var heartSix: Array = [3,5];
//private static var heartSeven: Array = [3,6];
//private static var heartEight: Array = [3,7];
private static var heartNine: Array = [3,8];
private static var heartTen: Array = [3,9];
private static var heartJack: Array = [3,10];
private static var heartQueen: Array = [3,11];
private static var heartKing: Array = [3,12];

//private static var cards: Array = [
//	spadeAce,spadeTwo,spadeThree,spadeFour,
//	spadeFive,spadeSix,spadeSeven,spadeEight,
//	spadeNine,spadeTen,spadeJack,spadeQueen,spadeKing,
//	diamondAce,diamondTwo,diamondThree,diamondFour,
//	diamondFive,diamondSix,diamondSeven,diamondEight,
//	diamondNine,diamondTen,diamondJack,diamondQueen,diamondKing,
//	clubAce,clubTwo,clubThree,clubFour,
//	clubFive,clubSix,clubSeven,clubEight,
//	clubNine,clubTen,clubJack,clubQueen,clubKing,
//	heartAce,heartTwo,heartThree,heartFour,
//	heartFive,heartSix,heartSeven,heartEight,
//	heartNine,heartTen,heartJack,heartQueen,heartKing
//];

private static var cards: Array = [
	spadeAce,spadeNine,spadeTen,spadeJack,spadeQueen,spadeKing,
	diamondAce,diamondNine,diamondTen,diamondJack,diamondQueen,diamondKing,
	clubAce,clubNine,clubTen,clubJack,clubQueen,clubKing,
	heartAce,heartNine,heartTen,heartJack,heartQueen,heartKing
];
// 0 is ace, 12 is king
public function setCardFace(){
	var tex: Texture;
	//var index: int = Random.Range(0,cards.length);
	var index: int = 0;
	var card: Array = cards[index];
	cards.RemoveAt(index);
	//Debug.Log("index" + index);
	var suit: int = card[0];
	var val: int = card[1];
	
	switch(suit) {
		case 0:
			tex = this.spades[val];
			name = (suit*13 + val).ToString();
			break;
		case 1:
			tex = this.hearts[val];
			name = (suit*13 + val).ToString();
			break;
		case 2:
			tex = this.clubs[val];
			name = (suit*13 + val).ToString();
			break;
		case 3:
			tex = this.diamonds[val];
			name = (suit*13 + val).ToString();
			break;
		default:
			//default to ace of spades
			tex = this.spades[0];	
			name = '0';	  
	}
	
	return tex;
}

function Awake(){
	var rotate = new Quaternion(0,180,0,0);
	transform.localEulerAngles.y += 180;
	//gameObject.renderer.material.mainTexture = setCardFace();
	transform.Find("Bottom").renderer.material.mainTexture = setCardFace();
	
	Random.seed = 3;
}

//not the auto updating function. Change to cap Update to cycle through automatically. 
//It's currently called from the user input functions
function update(){
	networkView.RPC("updatePos", RPCMode.Others, name, transform.position);
}

@RPC
function updatePos(cardName: String, pos: Vector3){
	var crd: GameObject = GameObject.Find(cardName);

	if(crd != null){
		crd.transform.position = pos;
	}
}

function updateRotation(){
	networkView.RPC("updateRot", RPCMode.Others, name, transform.eulerAngles);
}

@RPC
function updateRot(cardName: String, rot: Vector3){
	var crd: GameObject = GameObject.Find(cardName);

	if(crd != null){
		crd.transform.rotation.eulerAngles.x = rot.x;
		crd.transform.rotation.eulerAngles.y = rot.y;
		crd.transform.rotation.eulerAngles.z = rot.z;
	}
}

/*@RPC
function updateOnServer(cardName: String, pos: Vector3){
	networkManage.GetComponent(NetworkManager).updateCard(cardName, pos);
}*/

@RPC 
public function SetPosition(newPosition: Vector3) {
	transform.position = newPosition;
}
@RPC
public function SetRotation(newPosition:float) {
	gameObject.transform.rotation.eulerAngles.y = newPosition;
}
@RPC
public function SetRotation(x:float, y:float, z:float) {
	transform.rotation.eulerAngles.x = x;
	transform.rotation.eulerAngles.y = y;
	transform.rotation.eulerAngles.z = z;
	
	updateRotation();
}

/*@RPC 
public function MakeFaceDown(newRotation: float) {
	if (isInHand) {
		transform.rotation.eulerAngles.y = newRotation;
	}
}*/

//Touch Controls
/*
private var lastTouchTime: float = 0;

function OnTouchBegan (){
	if(Time.time - lastTouchTime < catchTime){
		//double touch
		transform.rotation = Quaternion.Euler(0, 180,  180);
    }
    else {
		moveBegin(Input.mousePosition);
    }
    lastTouchTime = Time.time;
}

function OnTouchMoved (){
	moving(Input.GetTouch(0).position);
}

function OnTouchStationary (){
	createCardPile();
	movePile = true;
	liftPile = true;
	//moveBegin(Input.GetTouch(0).position);
}

function OnTouchEnded (){
	moveEnd(Input.GetTouch(0).position);
    clearCardPiles();
    movePile = false;
}
*/

//Mouse Controls
private var lastClickTime: float = 0;
private var catchTime: float = .25;
private var prevMousePosition: Vector3;
private var deltaMousePosition: Vector3;
private var minDelta = new Vector3(0.001, 0.001, 0.001);
private var startSingleMove = true;

function OnMouseDown (){

	//Rotate Card to the player view
      var ang:int = Camera.main.transform.eulerAngles.y;
	
      transform.rotation = Quaternion.Euler(0, ang, transform.eulerAngles.z);
      networkView.RPC("updateRot", RPCMode.Others, name, new Vector3(0,ang,transform.eulerAngles.z));
      update();
    
	if(Time.time - lastClickTime < catchTime){
		//double click
		//transform.rotation.eulerAngles.y += 180;
		transform.rotation.eulerAngles.z += 180;
		
		
		//TODO need to check if this happens in the hand. if it does, don't flip it. and show to everyone. 
		
		networkView.RPC("updateRot", RPCMode.Others, name, new Vector3(0,ang,transform.eulerAngles.z));
		// += Quaternion.Euler(0, 180,  180);
		doDrop = true;
		/*
		createCardPile();
		movePile = true;
		liftPile = true;
		moveBegin(Input.mousePosition);
		print("doubleclick:" + (Time.time - lastClickTime).ToString());
		*/
    }
    else {
		moveBegin(Input.mousePosition);
    }
    prevMousePosition = Input.mousePosition;
    lastClickTime = Time.time;
}
 
function OnMouseDrag (){
    if (Time.time - lastClickTime > .3 && mouseUnmoved(Input.mousePosition - prevMousePosition) && canCreatePile) {
    	//click and hold detected
    	createCardPile();
		movePile = true;
		liftPile = true;
		canCreatePile = false;
    }
   	moving(Input.mousePosition);
}
 
function mouseUnmoved (deltaMousePosition: Vector3) {
	if (deltaMousePosition.x - minDelta.x < 0.01 && deltaMousePosition.y - minDelta.y < 0.01 && deltaMousePosition.z - minDelta.z < 0.01) {
		return true;
	}
	return false;
} 
 
function OnMouseUp(){
	//Debug.Log("MouseUp");
    moveEnd(Input.mousePosition);
    clearCardPiles();
    movePile = false;
}




function moveBegin(pos: Vector3) {
	var viewID = Network.AllocateViewID();
	
	screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
    offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, screenPoint.z));

    transform.position += offset;
 //   transform.position.y += lift;
    transform.localScale.x += lift;
    transform.localScale.z += lift;
    Screen.showCursor = false;
    //bring the card we moved to the top plane and move all cards above this card down one plane
    var game: Object[] = GameObject.FindObjectsOfType(typeof(MonoBehaviour)) as Object[];
	for(var j = 0; j < game.length; j++){
		var cur: Card = game[j] as Card;  //attempts to cast all objects as cards
		if(cur != null){
      		if (cur.getPos().y > transform.position.y) {
      			//this card is above the card we moved, move it down
      			cur.setPos(cur.getPos().x, cur.getPos().y - .15, cur.getPos().z);
      		}			
   		}
   	}
	transform.position.y = .5*24;
    
    update();
}

function moving(pos: Vector3) {
	//update previous position so we can calculate delta
	prevPosition = curPosition;
	curScreenPoint = new Vector3(pos.x, pos.y, screenPoint.z);
    curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
    transform.position = curPosition;
    //delta is the vector we wish to apply to each card in the card pile
    delta = curPosition - prevPosition;
    if (movePile) {
		for (var i = 0; i < cardPile.length; i++) {
			if (cardPile[i] != this) {
				var cardTemp = cardPile[i] as Card;
				cardTemp.transform.position += delta;
				if (liftPile) {
				    cardTemp.transform.localScale.x += lift;
    				cardTemp.transform.localScale.z += lift;
    			}
    			cardTemp.update();
			}
		}
	}
	transform.position.y = .5*24;
	liftPile = false;
    update();
}

function moveEnd(pos: Vector3) {
	Screen.showCursor = true;
	//check if we have just flipped a card, not moved it (i.e. we don't need to drop it)
	if (!doDrop) {
		transform.localScale.x -= lift;
		transform.localScale.z -= lift;		
	}
	//drop all the cards that we picked up with the card pile
	for (var i = 0; i < cardPile.length; i++) {
		var card = cardPile[i] as Card;
		if (cardPile[i] != this) {
			card.transform.localScale.x -= lift;
  			card.transform.localScale.z -= lift;
   		}
   	} 
    doDrop = false;
    canCreatePile = true;
    transform.position.y = .5*24;
    //Andrew HEY!!

    //check if over player
    
    /*
    
    var temp: GameObject[];
    temp = GameObject.FindGameObjectsWithTag("Player");
    
    Debug.Log("Card z: " + gameObject.transform.position.z);
    Debug.Log("Plane z: " + temp[0].transform.position.z);
    

    if(gameObject.transform.position.z - 2 <= temp[0].transform.position.z){
    	Debug.Log("CHECKING: " + temp[0].tag + " END");
   		Debug.Log("Send to Player");
		temp[0].GetComponent(Player1).addCard(gameObject);
    
    }
    
    */

    //update();
}
/*
function OnSerializeNetworkView(stream:BitStream, info:NetworkMessageInfo)
{
	var syncpos : Vector3 = Vector3.zero;
	
	if (stream.isWriting) {
		//Debug.Log("stream is writing");
	    syncpos = transform.position;
	    stream.Serialize(syncpos);
	} else {
		//Debug.Log("stream is reading");
	    stream.Serialize(syncpos);
	    transform.position = syncpos;
	}   
}
*/

function setPos(x,y,z){
	transform.position = new Vector3(x, y, z);
	update();
}

function getPos() : Vector3{
	return transform.position;
}

function getChecked() {
	return isChecked;
}

function setIsChecked(val) {
	if (val == true || val == false) {
		isChecked = val;
	}
}

/*function setIsInHand(val) {
	isInHand = val;
}*/

function createCardPile (){
	//iterate through game objects and find cards
	var game: Object[] = GameObject.FindObjectsOfType(typeof(MonoBehaviour)) as Object[];
	for(var j=0; j < game.length; j++){
		var cur: Card = game[j] as Card;  //attempts to cast all objects as cards
		if(cur != null){
			cur.setIsChecked(false);
		}
	}
	
	cardPile = fillCardPile();
	Debug.Log("Cardpile size: " + cardPile.length);
}

function fillCardPile() : Array {
	//Iterate through all cards in the game
	/*another option:
	/ Have an array to keep track of the positions of every card.
	/ Iterate through this array and determine if cards are overlapping.
	/
	*/
	isChecked = true;
	var pile = new Array();
	var arrs = new Array();
	arrs.Push(this);
	var game: Object[] = GameObject.FindObjectsOfType(typeof(MonoBehaviour)) as Object[];
	for(var j = 0; j < game.length; j++){
		var cur: Card = game[j] as Card;
		//check if object is null, if not: is it unchecked
		if(cur != null && !cur.getChecked()){
			//the current object is a card and unchecked, check if it's overlapping
			if (checkOverlap(cur)) {
				//it is overlapping:
				//recursively call createCardPile() on current card
				arrs = arrs.Concat(cur.fillCardPile());
			}			
		}
	}
	for (var i = 0; i < arrs.length; i++) {
		var found = false;
		for (var k = 0; k < pile.length; k++) {
			if (arrs[i] == pile[k]) {
				found = true;
			}
		}
		if (!found) {
			pile.Push(arrs[i]);
		}
	}
	return pile;
}

function checkOverlap(card:Card) {
	//Create var for position of card
	var curPos: Vector3;
	curPos = card.getPos();
	
	//Setup edges vars
	//CardA: this card
	//CardB: card param
	//Debug.Log(transform.localScale);
	//Debug.Log(transform.localScale.x);
	//Debug.Log(transform.localScale.x/2);
	var leftEdgeA	= transform.position.x-(transform.localScale.x/2);
	var leftEdgeB	= curPos.x-(transform.localScale.x/2);
	var rightEdgeA	= transform.position.x+(transform.localScale.x/2);
	var rightEdgeB	= curPos.x+(transform.localScale.x/2);
	var topEdgeA	= transform.position.z+(transform.localScale.z/2);
	var topEdgeB	= curPos.z+(transform.localScale.z/2);
	var bottomEdgeA = transform.position.z-(transform.localScale.z/2);
	var bottomEdgeB = curPos.z-(transform.localScale.z/2);
	
	// check if found card overlaps with this card
	if ((rightEdgeB >= leftEdgeA && rightEdgeB <= rightEdgeA) || (leftEdgeB <= rightEdgeA && leftEdgeB >= leftEdgeA)) {
		//vertical edges are in overlapping region
		if ((topEdgeB >= bottomEdgeA && topEdgeB <= topEdgeA) || (bottomEdgeB <= topEdgeA && bottomEdgeB >= bottomEdgeA)) {
			//horizontal edges are in overlapping region
			//This is an overlapping case !!!!
			return true;		
		}			
	}
	//corner case where horizontal edges are touching
	else if (topEdgeA == bottomEdgeB || bottomEdgeA == topEdgeB) {
		return true;
	}
	//The cards can't possibly overlap
	return false;			
}


function emptyCardPile() {
	cardPile.Clear();
	//Debug.Log("cardPile now has: " + cardPile.length);
}

function clearCardPiles() {
	var game: Object[] = GameObject.FindObjectsOfType(typeof(MonoBehaviour)) as Object[];
	for(var j=0; j < game.length; j++){
		var curCard: Card = game[j] as Card;
		if (curCard != null) { 
			curCard.emptyCardPile();
		}	
	}	
}

/*
//Basic collision detection checking for two differently named objects
function OnCollisionEnter(theCollision : Collision){
	if(theCollision.gameObject.name == "Floor"){
		Debug.Log("Hit the floor");
	}
	else{
		Debug.Log("Didn't hit");
	}
}
*/

﻿#pragma strict

var MenuSkin : GUISkin;
var NetManager: NetworkManager;

var ShowMainMenu = true;
var HostToggle = false;
var JoinToggle = false;

var menu_width : int;
var menu_height : int;
var menu_width_edge_buffer : int;
var menu_height_edge_buffer : int;

var button_width_edge_buffer : int;
var button_width : int;
var button_height : int;

private var gameName: String = "Room Name";

function Start() {
	NetManager = GameObject.Find("Floor").GetComponent(NetworkManager);
	
	menu_width = Screen.width - 40;
	menu_height = Screen.height - 40;
	menu_width_edge_buffer = 20;
	menu_height_edge_buffer = 20;
	
	button_width_edge_buffer = 70;
	button_width = menu_width-2*button_width_edge_buffer;
	button_height = 75;
}

function OnGUI() {
	GUI.skin = MenuSkin;
	if (ShowMainMenu)
		MainMenuScreen();
	else if (HostToggle)
		HostMenuScreen();
	else if (JoinToggle)
		JoinMenuScreen();
}

function MainMenuScreen() {
	BuildBaseMenu("Perfect Deck of Cards");
	
	if (GUI.Button(Rect(button_width_edge_buffer,menu_height/2-(button_height+30),
		button_width,button_height),"Host New Game")) {
		HostToggle = true;
		ShowMainMenu = false;
	}
	
	if (GUI.Button(Rect(button_width_edge_buffer,menu_height/2+(30),
		button_width,button_height),"Join New Game")) {
		JoinToggle = true;
		ShowMainMenu = false;
	}
	
	GUI.EndGroup();
	GUI.EndGroup();
}

function HostMenuScreen() {
	BuildBaseMenu("Host A Game");
	
	gameName = GUI.TextArea(Rect(120,menu_height/2+(30),menu_width-240,25),gameName);
	
	if (GUI.Button(Rect(button_width_edge_buffer,menu_height/2-(button_height+30),
		button_width,button_height),"Open Room")) {
		HostToggle = false;
		NetManager.StartServer(gameName);
	}
	
	if (GUI.Button(Rect(0,0,100,50),"<<")) {
		ShowMainMenu = true;
		HostToggle = false;
	}
	
	GUI.EndGroup();
	GUI.EndGroup();
}

function JoinMenuScreen() {
	BuildBaseMenu("Join a Game");
	
	NetManager.RefreshHostList();
	var hostList : HostData[] = NetManager.GetHostList();
	if (hostList != null) {
		for (var i = 0; i < hostList.Length; i++) {
			//TODO deciding how the list should be placed determined by how many games are available is next
			//TODO scrollable list would be nice?
			//Game Label
			GUI.Label (Rect(button_width_edge_buffer ,(100+(i*button_height)) , (button_width/2) , button_height), hostList[i].gameName);
			
			//Join Game
			if (GUI.Button(Rect(button_width_edge_buffer + button_width/2,100+i*button_height,
				button_width/4,button_height),"Join")) {
				NetManager.JoinServer(hostList[i],0);
				JoinToggle = false;
			}
			
			//Spectate Game
			if (GUI.Button(Rect(button_width_edge_buffer + 2 + button_width/2 + (button_width/4) ,100+i*button_height,
				button_width/4,button_height),"Spectate")) {
				NetManager.JoinServer(hostList[i], 1);
				
				JoinToggle = false;
			}
		}
	}
	
	if (GUI.Button(Rect(0,0,100,50),"<<")) {
		ShowMainMenu = true;
		JoinToggle = false;	
	}
	
	GUI.EndGroup();
	GUI.EndGroup();
}

private function BuildBaseMenu(name : String) {
	GUI.BeginGroup(new Rect(0,0,Screen.width,Screen.height));
	var box : Rect = new Rect(menu_width_edge_buffer,menu_height_edge_buffer,
		menu_width,menu_height);
	GUI.Box(box,name);
	GUI.BeginGroup(box);
}

function reInit() {
	ShowMainMenu = true;
	HostToggle = false;
	JoinToggle = false;
}
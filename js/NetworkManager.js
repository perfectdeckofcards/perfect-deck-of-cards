#pragma strict

private var typeName: String = "PerfectDeckOfCards";
private var gameName: String = "Room Name";
private var hostList: HostData[];
private var cards: GameObject[];
private var clientcount: int = 0;
private var MAX_CONNECTIONS: int = 4;
private var MAX_CARDS: int = 24;
private var Menu: MainMenu;
private var ang:int;

var isSpectator = false;
var GameReady = false;

function Start() 
{
	Menu = GameObject.Find("Main Camera").GetComponent(MainMenu);
	
	//MasterServer.ipAddress = "72.33.29.168";
	
}

function OnGUI() {
	if (GameReady) {
		if (GUI.Button(Rect(100, 100, 125, 50), GUIContent("Shuffle"))){
			if(Network.isClient){
				networkView.RPC("shuffle", RPCMode.All);
			}
			
			else {
				shuffle();
			}	
		}
		
		if (GUI.Button(Rect(0,0,75,50), "<<"))
			DestroyGame();
	}
}

public function StartServer(name: String)
{
	GameReady = true;
	Network.InitializeSecurity();
	//Network.InitializeServer(MAX_CONNECTIONS, 25001, !Network.HavePublicAddress());
	Network.InitializeServer(MAX_CONNECTIONS, 25001, Network.HavePublicAddress());
	MasterServer.RegisterHost(typeName, name);
	
	//Create new Player
	
	var temp: GameObject = Instantiate(player, new Vector3(0, 0, 0), Quaternion.Euler(0, 0,  0));
	temp.transform.Find("name").GetComponent(TextMesh).text = "Player 1";
	temp.name = "Player 1";
//	temp.Find("Player 1").GetComponent(Player1).setAngle(0);
}

private function DestroyGame() {
	Menu.reInit();
	Debug.Log(Network.connections.Length);
	if (Network.connections.Length > 0) {
	//disconnects players from game, moving cards will no longer propogate to other players
		for(var i = 0; i < Network.connections.Length; i++) {
			Debug.Log("Disconnecting: "+
			Network.connections[i].ipAddress+":"+Network.connections[i].port);
			Network.CloseConnection(Network.connections[i], true);
		}
	} else {
		Debug.Log("No players are connected");
	}
	
	GameReady = false;
}

public function GetHostList() {
	return hostList;
}

function RefreshHostList()
{
	MasterServer.RequestHostList(typeName);
}

function OnMasterServerEvent(msEvent: MasterServerEvent)
{
	if (msEvent == MasterServerEvent.HostListReceived)
		hostList = MasterServer.PollHostList();
}

public var hider:GameObject;

function JoinServer(hostData: HostData, mode:int)
{
	if (clientcount < 3) {
		var temp : GameObject;
		if(mode == 1){
			Debug.Log("Mode On");
			temp = GameObject.Find("Floor");
			temp.GetComponent(NetworkManager).setSpec();
			GameObject.Instantiate(hider);
			Network.Connect(hostData);
		}
		else{
			Network.Connect(hostData);
		}
		clientcount++;
	}
}

function setSpec(){
	if(isSpectator){
		isSpectator = false;
	}
	else{
		isSpectator = true;
	}
}

function OnServerInitialized()
{
	SpawnDeck();
}

function OnConnectedToServer()
{
	if(isSpectator){
		networkView.RPC("RequestExistingPlayers", RPCMode.Server);
		SpawnDeck();
		Debug.Log("Connected!");
		setSpec();
		
	}
	else{


	networkView.RPC("RequestExistingPlayers", RPCMode.Server);
	networkView.RPC("SetPlayer", RPCMode.Server);
	networkView.RPC("RequestCameraSet", RPCMode.Server);
	SpawnDeck();
	}
	
}

@RPC
function RequestExistingPlayers(info : NetworkMessageInfo) {
	networkView.RPC("SetExistingPlayers", info.sender, Network.connections.Length);
}

@RPC
function SetExistingPlayers(num: int){
	for(var i = 0; i < num; i++){
		var ang = -90*i;
		var temp: GameObject = Instantiate(player, new Vector3(0, 0, 0), Quaternion.Euler(0, ang,  0));
		temp.transform.Find("name").GetComponent(TextMesh).text = "Player " + (i+1);
		temp.name = "Player " + (i+1);
//		temp.Find(temp.name).GetComponent(Player1).setAngle(ang);
	}
}

function OnNetworkInstantiate(info: NetworkMessageInfo) {
	Debug.Log(networkView.viewID + " spawned");
}

/*
function OnSerializeNetworkView(stream: BitStream, info: NetworkMessageInfo)
{

	var sync: Vector3 = Vector3.zero;
	if (stream.isWriting) 
	{
		sync = card.rigidbody.position;
		stream.Serialize(sync);
	} else 
	{
		stream.Serialize(sync);
		card.rigidbody.position = sync;
	}
}
*/

@RPC
public function RequestCameraSet(info : NetworkMessageInfo) {
	networkView.RPC("SetCamera", info.sender, Network.connections.Length);
}

@RPC
public function SetCamera(size : int) {
	ang = -90*(size);
	Debug.Log(ang);
	
	GameObject.Find("Main Camera").transform.eulerAngles.y = ang;
}

@RPC
public function SetPlayer() {
	if(Network.isServer){
		networkView.RPC("CreatePlayerOnAll", RPCMode.All, Network.connections.Length);
	}
}

@RPC
public function CreatePlayerOnAll(size: int) {
	var ang = -90*size;
	var temp: GameObject = Instantiate(player, new Vector3(0, 0, 0), Quaternion.Euler(0, ang,  0));
	temp.transform.Find("name").GetComponent(TextMesh).text = "Player " + (size+1);
	temp.name = "Player " + (size+1);
}

public var card: GameObject;
public var player: GameObject;
public function SpawnDeck() {
	Random.seed = 1;  
	for(var i = 0; i < 24; i++) {
		Instantiate(card, new Vector3(0, (24-i) * .5, 0), Quaternion.identity);
	}
}
/*
@RPC
public function updateCard(cardName: String, pos: Vector3){
	
	var crd: GameObject = GameObject.Find(cardName);

	Debug.Log(crd);

	if(crd != null){
		crd.transform.position = pos;
	}
	
	if(Network.isServer){
		networkView.RPC("updateCard", RPCMode.Others, cardName, pos);
	}
}
*/

//shuffles the specified number of cards (52 for one standard deck)
@RPC
public function shuffle() {
	Debug.Log("Shuffling...");
	
	var order: Array = new Array();
	for(var i=0; i < 24; i++){
		order.push(.5*i);
	}
	order = shuffleArray(order);
	
	var game: Object[] = GameObject.FindObjectsOfType(typeof(MonoBehaviour)) as Object[];
	var count = 0;
	for(var j=0; j < game.length; j++){
		var cur: Card = game[j] as Card;  //attempts to cast all objects as cards
		if(cur != null){
			cur.setPos(0,order[count],0);
			cur.SetRotation(0, 180, 0);
			count++;
		}
	}
}

public function shuffleArray(arr:Array) {
	var i = arr.length-1;
		
	while(i != 0){
		var j = Random.Range(0,i);
		var x = arr[i];
		arr[i] = arr[j];
		arr[j] = x;
		i -= 1;
	}
	return arr;
}

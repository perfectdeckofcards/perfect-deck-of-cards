This repository should be installed in your unity projects assets folder under the correct scene. For me this is /Cards/Assets. We will be holding all assets here.

The repo contains folders for javascript, materials, textures, and prefabs.

Install:

**1) Download and install unity**

**2) Create Card project**

**3) Clone the repository into the assets folder**

~~~~
git clone git@bitbucket.org:perfectdeckofcards/perfect-deck-of-cards.git
~~~~

Then open the single scen from the scenes folder inside the projects view in Unity. The application should run normally if you hit the play button.

You can run two instances by buidling the application and also running it in Unity. You should then be able to start a server in one window and join that server as a client from the other. 

Testing section:
In order to use the unit tests we have developed, the Unity Test Tools asset must be downloaded and installed from the Unity 
Asset Store.  It will automatically plugin to the project and no further setup is needed.

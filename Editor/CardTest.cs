﻿using NUnit.Framework;
using System;
using UnityEngine;
using System.Collections;

public class CardTest : MonoBehaviour {


	/// <summary>
	/// Tests overlap detection method in Card.js.
	/// First test set checks proper use of method
	/// Second test set uses only 2 cards.
	/// Third test set uses full 52 cards.
	/// </summary>

	///////////////////
	/// Test Set #1 ///
	///////////////////
	/// <summary>
	/// Tests the proper call of checkOverlap().
	/// </summary>
	[Test]
	public void testOverlapProperCall()
	{
		GameObject card1;
		GameObject card2;
		card1 = (GameObject) Instantiate(Resources.Load("Card"));
		card2 = (GameObject) Instantiate(Resources.Load("Card"));
		
		//call overlap detection method

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);
	}

	///////////////////
	/// Test Set #2 ///
	/// -Two Cards- ///
	///////////////////
	/// <summary>
	/// Two overlapping cards in the exact same position(x and z)
	/// </summary>
	[Test]
	public void testOverlapSamePos2()
	{
		/*
		//Card card1, card2;
		GameObject card1 = Instantiate(Resources.Load("Card"));
		card1.GetComponent<Card>();
		//Card card2 = Instantiate(Resources.Load("Card"));
		
		//call overlap detection method
		Assert.That (card1.checkOverlap(card2));

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);*/
	}

	/// <summary>
	/// Two overlapping cards with left/right side overlapping
	/// </summary>
	[Test]
	public void testOverlapLeftRigh2()
	{
		/*Card card1;
		Card card2;
		card1 = (Card) Instantiate(Resources.Load("Card"));
		card2 = (Card) Instantiate(Resources.Load("Card"));

		card1.transform.position += new Vector3 (1, 0, 0);

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);*/
	}

	/// <summary>
	/// Two overlapping cards with right/left side overlapping
	/// </summary>
	[Test]
	public void testOverlapRightLeft2()
	{
		GameObject card1;
		GameObject card2;
		card1 = (GameObject) Instantiate(Resources.Load("Card"));
		card2 = (GameObject) Instantiate(Resources.Load("Card"));
		
		card1.transform.position += new Vector3 (1, 0, 0);

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);
	}

	/// <summary>
	/// Two overlapping cards with top/bottom overlapping
	/// </summary>
	[Test]
	public void testOverlapTopBottom2()
	{
		GameObject card1;
		GameObject card2;
		card1 = (GameObject) Instantiate(Resources.Load("Card"));
		card2 = (GameObject) Instantiate(Resources.Load("Card"));
		
		card1.transform.position += new Vector3 (1, 0, 0);

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);
	}

	/// <summary>
	/// Two overlapping cards with bottom/top overlapping
	/// </summary>
	[Test]
	public void testOverlapBottomTop2()
	{
		GameObject card1;
		GameObject card2;
		card1 = (GameObject) Instantiate(Resources.Load("Card"));
		card2 = (GameObject) Instantiate(Resources.Load("Card"));
		
		card1.transform.position += new Vector3 (1, 0, 0);

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);
	}

	/// <summary>
	/// Two cards just touching on edges, should count as overlapping
	/// left/right
	/// </summary>
	[Test]
	public void testOverlapEdgesLR2()
	{
		GameObject card1;
		GameObject card2;
		card1 = (GameObject) Instantiate(Resources.Load("Card"));
		card2 = (GameObject) Instantiate(Resources.Load("Card"));
		
		card1.transform.position += new Vector3 (1, 0, 0);

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);
	}

	/// <summary>
	/// Two cards just touching on edges, should count as overlapping
	/// right/left
	/// </summary>
	[Test]
	public void testOverlapEdgesRL2()
	{
		GameObject card1;
		GameObject card2;
		card1 = (GameObject) Instantiate(Resources.Load("Card"));
		card2 = (GameObject) Instantiate(Resources.Load("Card"));
		
		card1.transform.position += new Vector3 (1, 0, 0);

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);
	}

	/// <summary>
	/// Two cards just touching on edges, should count as overlapping
	/// top/bottom
	/// </summary>
	[Test]
	public void testOverlapEdgesTB2()
	{
		GameObject card1;
		GameObject card2;
		card1 = (GameObject) Instantiate(Resources.Load("Card"));
		card2 = (GameObject) Instantiate(Resources.Load("Card"));
		
		card1.transform.position += new Vector3 (1, 0, 0);

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);
	}

	/// <summary>
	/// Two cards just touching on edges, should count as overlapping
	/// bottom/top
	/// </summary>
	[Test]
	public void testOverlapEdgesBT2()
	{
		GameObject card1;
		GameObject card2;
		card1 = (GameObject) Instantiate(Resources.Load("Card"));
		card2 = (GameObject) Instantiate(Resources.Load("Card"));
		
		card1.transform.position += new Vector3 (1, 0, 0);

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);
	}

	/// <summary>
	/// Two cards that are not overlapping
	/// </summary>
	[Test]
	public void testOverlapFalse2()
	{
		GameObject card1;
		GameObject card2;
		card1 = (GameObject) Instantiate(Resources.Load("Card"));
		card2 = (GameObject) Instantiate(Resources.Load("Card"));
		
		card1.transform.position += new Vector3 (1, 0, 0);

		//Teardown
		DestroyImmediate (card1);
		DestroyImmediate (card2);
	}

	[Test]
	public void testCreateCardpile() {

	}

	[Test]
	public void SimpleAddition()
	{
		Assert.That (2 + 2 == 4);
	}

}
